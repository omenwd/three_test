import {useState, useEffect} from "react";
import * as THREE from "three";
import {OrbitControls} from "three/examples/jsm/controls/OrbitControls"
import { GLTFLoader } from "three/examples/jsm/loaders/GLTFLoader";

const raycaster = new THREE.Raycaster();
const pointer = new THREE.Vector2();
document.addEventListener( 'pointerdown', onPointerMove );
function onPointerMove( event ) {
  console.log("move");
  pointer.x = ( event.clientX / window.innerWidth ) * 2 - 1;
  pointer.y = - ( event.clientY / window.innerHeight ) * 2 + 1;
  console.log(pointer);
}

function getAllMeshes(object) {
  return object.children[0].children[0].children[0].children[0].children
}


export default function Canvas() {
  const [canvasLoaded, setCanvasLoaded] = useState();

  useEffect(() => {
    if (!canvasLoaded) {
      console.log("setCamera");
      setCamera();
    }
  }, [canvasLoaded]);

  const setCamera = () => {
    const canvas = document.getElementById("my-canvas");
    const renderer = new THREE.WebGLRenderer({canvas});
    if (canvas) {
      setCanvasLoaded();
      const fov = 45;
      const aspect = 2;  // the canvas default
      const near = 0.1;
      const far = 100;
      const camera = new THREE.PerspectiveCamera(fov, aspect, near, far);
      camera.position.set(0, 10, 20);
      const controls = new OrbitControls(camera, canvas);
      controls.target.set(0, 5, 0);
      controls.update();
      const scene = new THREE.Scene();
      scene.background = new THREE.Color('aliceblue');

      {
        const planeSize = 40;
        const loader = new THREE.TextureLoader();
        const texturesArray = {
          't1': {
            url: '/static/argo_container/textures/material_baseColor.png',
            val: undefined
          },
          't2': {
            url: '/static/argo_container/textures/material_metallicRoughness.png',
            val: undefined
          },
          't3': {
            url: '/static/argo_container/textures/material_normal.png',
            val: undefined
          }
        };
        const texturePromises = [];
        for (const key in texturesArray) {
          texturePromises.push(new Promise((resolve, reject) => {
            var entry = texturesArray[key]
            var url = entry.url
            loader.load(url,
              texture => {
                entry.val = texture;
                if (entry.val instanceof THREE.Texture) resolve(entry);
              },
              xhr => {
                console.log(url + ' ' + (xhr.loaded / xhr.total * 100) +
                  '% loaded');
              },
              xhr => {
                reject(new Error(xhr +
                  'An error occurred loading while loading: ' +
                  entry.url));
              }
            );
          }));
        }
        Promise.all(texturePromises).then(loadedTextures => {
          const planeGeo = new THREE.PlaneGeometry(planeSize, planeSize);
          const planeMat = new THREE.MeshPhongMaterial({
            t1: texturesArray.t1.val,
            t2: texturesArray.t2.val,
            t3: texturesArray.t3.val,
            side: THREE.DoubleSide,
          });
          const mesh = new THREE.Mesh(planeGeo, planeMat);
          mesh.rotation.x = Math.PI * -.5;
          scene.add(mesh);
        });
      }
      {
        const skyColor = 0xB1E1FF;  // light blue
        const groundColor = 0xB97A20;  // brownish orange
        const intensity = 1;
        const light = new THREE.HemisphereLight(skyColor, groundColor, intensity);
        scene.add(light);
      }
      {
        const color = 0xFFFFFF;
        const intensity = 1;
        const light = new THREE.DirectionalLight(color, intensity);
        light.position.set(5, 10, 2);
        scene.add(light);
        scene.add(light.target);
      }
      function frameArea(sizeToFitOnScreen, boxSize, boxCenter, camera) {
        const halfSizeToFitOnScreen = sizeToFitOnScreen * 0.5;
        const halfFovY = THREE.MathUtils.degToRad(camera.fov * .5);
        const distance = halfSizeToFitOnScreen / Math.tan(halfFovY);
        // compute a unit vector that points in the direction the camera is now
        // in the xz plane from the center of the box
        const direction = (new THREE.Vector3())
          .subVectors(camera.position, boxCenter)
          .multiply(new THREE.Vector3(1, 0, 1))
          .normalize();

        // move the camera to a position distance units way from the center
        // in whatever direction the camera was from the center already
        camera.position.copy(direction.multiplyScalar(distance).add(boxCenter));

        // pick some near and far values for the frustum that
        // will contain the box.
        camera.near = boxSize / 100;
        camera.far = boxSize * 100;

        camera.updateProjectionMatrix();

        // point the camera to look at the center of the box
        camera.lookAt(boxCenter.x, boxCenter.y, boxCenter.z);
      }
      {
        const gltfLoader = new GLTFLoader();
        gltfLoader.load('/static/argo_container/scene.gltf', (gltf) => {
          const root = gltf.scene;
          root.name="my_scene";
          scene.add(root);

          // compute the box that contains all the stuff
          // from root and below
          const box = new THREE.Box3().setFromObject(root);

          const boxSize = box.getSize(new THREE.Vector3()).length();
          const boxCenter = box.getCenter(new THREE.Vector3());

          // set the camera to frame the box
          frameArea(boxSize * 0.5, boxSize, boxCenter, camera);

          // update the Trackball controls to handle the new size
          controls.maxDistance = boxSize * 10;
          controls.target.copy(boxCenter);
          controls.update();
        });
      }
      function resizeRendererToDisplaySize(renderer) {
        const canvas = renderer.domElement;
        const width = canvas.clientWidth;
        const height = canvas.clientHeight;
        const needResize = canvas.width !== width || canvas.height !== height;
        if (needResize) {
          renderer.setSize(width, height, false);
        }
        return needResize;
      }

      function render() {
        if (resizeRendererToDisplaySize(renderer)) {
          const canvas = renderer.domElement;
          camera.aspect = canvas.clientWidth / canvas.clientHeight;
          camera.updateProjectionMatrix();
        }
        raycaster.setFromCamera( pointer, camera );
        // console.log(scene.children);
        // console.log(scene);
        if (scene.getObjectByName('my_scene')) {
          const children = getAllMeshes(scene.getObjectByName('my_scene'));
          const intersects = raycaster.intersectObjects(children, true);
          if (intersects.length) {
            console.log({ intersects });
            children.forEach((item, key) => {
              if (intersects.findIndex((int) => int.object.name === item.name) === -1) {
                children[key].children[0].material.color.set("black");
              }
            })
            for ( let i = 0; i < intersects.length; i ++ ) {
              intersects[ i ].object.material.color.set( "red" );
              /*const sphereGeometry = new THREE.SphereGeometry( 120, 32, 32 );
              const sphereMaterial = new THREE.MeshBasicMaterial( { color: "yellow" } );
              const sphere =  new THREE.Mesh( sphereGeometry, sphereMaterial );
              console.log(intersects[ i ].object.position, intersects[ i ].object)
              sphere.position.set(intersects[ i ].object.position);
              scene.add( sphere );*/
              // console.log("!");
            }
          }
        }

        renderer.render(scene, camera);

        requestAnimationFrame(render);
      }

      requestAnimationFrame(render);
    }
  }
  return (
    <canvas id="my-canvas" width="600" height="450"></canvas>
  )
}
