import * as THREE from "three";
import {OrbitControls} from "three/examples/jsm/controls/OrbitControls"
import {GLTFLoader} from "three/examples/jsm/loaders/GLTFLoader";
import {GUI} from "three/examples/jsm/libs/dat.gui.module";

const container = document.getElementById( 'container' );

let renderer, scene, camera;
let mesh;
let raycaster;
let line;

const intersection = {
  intersects: false,
  point: new THREE.Vector3(),
  normal: new THREE.Vector3()
};
const mouse = new THREE.Vector2();
const intersects = [];

const textureLoader = new THREE.TextureLoader();

const decals = [];
let mouseHelper;
const position = new THREE.Vector3();
const orientation = new THREE.Euler();
const size = new THREE.Vector3( 10, 10, 10 );

const params = {
  minScale: 10,
  maxScale: 20,
  rotate: true,
  clear: function () {

    removeDecals();

  }
};

window.addEventListener( 'load', init );

function init() {

  renderer = new THREE.WebGLRenderer( { antialias: true } );
  renderer.setPixelRatio( window.devicePixelRatio );
  renderer.setSize( window.innerWidth, window.innerHeight );
  container.appendChild( renderer.domElement );

  scene = new THREE.Scene();

  camera = new THREE.PerspectiveCamera( 45, window.innerWidth / window.innerHeight, 1, 1000 );
  camera.position.z = 120;

  const controls = new OrbitControls( camera, renderer.domElement );
  controls.minDistance = 50;
  controls.maxDistance = 200;

  scene.add( new THREE.AmbientLight( 0x443333 ) );

  const dirLight1 = new THREE.DirectionalLight( 0xffddcc, 1 );
  dirLight1.position.set( 1, 0.75, 0.5 );
  scene.add( dirLight1 );

  const dirLight2 = new THREE.DirectionalLight( 0xccccff, 1 );
  dirLight2.position.set( - 1, 0.75, - 0.5 );
  scene.add( dirLight2 );

  const geometry = new THREE.BufferGeometry();
  geometry.setFromPoints( [ new THREE.Vector3(), new THREE.Vector3() ] );

  line = new THREE.Line( geometry, new THREE.LineBasicMaterial() );
  scene.add( line );

  loadModel();

  raycaster = new THREE.Raycaster();

  mouseHelper = new THREE.Mesh( new THREE.BoxGeometry( 1, 1, 10 ), new THREE.MeshNormalMaterial() );
  mouseHelper.visible = false;
  scene.add( mouseHelper );

  window.addEventListener( 'resize', onWindowResize );

  let moved = false;

  controls.addEventListener( 'change', function () {
    moved = true;
  } );

  window.addEventListener( 'pointerdown', function () {
    moved = false;
  });

  window.addEventListener( 'pointerup', function ( event ) {
    if ( moved === false ) {
      checkIntersection( event.clientX, event.clientY );
      if ( intersection.intersects ) {
        shoot();
        showLengths();
      }
    }
  });

  window.addEventListener( 'pointermove', onPointerMove );

  function onPointerMove( event ) {
    if ( event.isPrimary ) {
      checkIntersection( event.clientX, event.clientY );
    }
  }

  function checkIntersection( x, y ) {
    if ( mesh === undefined ) return;
    mouse.x = ( x / window.innerWidth ) * 2 - 1;
    mouse.y = - ( y / window.innerHeight ) * 2 + 1;
    raycaster.setFromCamera( mouse, camera );
    raycaster.intersectObject( mesh, true, intersects );

    if ( intersects.length > 0 ) {
      const p = intersects[ 0 ].point;
      mouseHelper.position.copy( p );
      intersection.point.copy( p );

      const n = intersects[ 0 ].face.normal.clone();
      n.transformDirection( mesh.matrixWorld );
      n.multiplyScalar( 10 );
      n.add( intersects[ 0 ].point );

      intersection.normal.copy( intersects[ 0 ].face.normal );
      mouseHelper.lookAt( n );

      const positions = line.geometry.attributes.position;
      positions.setXYZ( 0, p.x, p.y, p.z );
      positions.setXYZ( 1, n.x, n.y, n.z );
      positions.needsUpdate = true;
      intersection.intersects = true;
      intersects.length = 0;
    } else {
      intersection.intersects = false;
    }

  }

  const gui = new GUI();

  gui.add( params, 'minScale', 1, 10 );
  gui.add( params, 'maxScale', 1, 50 );
  gui.add( params, 'rotate' );
  gui.add( params, 'clear' );
  gui.open();

  onWindowResize();
  animate();

}

function loadModel() {
  const loader = new GLTFLoader();
  loader.load( '/static/data_set/3d_mesh/mesh.glb', function ( gltf ) {
    mesh = gltf.scene.children[ 0 ];
    mesh.material = new THREE.MeshPhongMaterial( {
      specular: 0x111111,
      map: textureLoader.load( '/static/data_set/3d_mesh/alwasil fort 3d_texture.jpg' ),
      specularMap: textureLoader.load( '/static/data_set/3d_mesh/alwasil fort 3d_texture.jpg' ),
      normalMap: textureLoader.load( '/static/data_set/3d_mesh/alwasil fort 3d_texture.jpg' ),
      shininess: 25
    });
    scene.add( mesh );
  });
}

function shoot() {
  position.copy( intersection.point );
  orientation.copy( mouseHelper.rotation );
  if ( params.rotate ) orientation.z = Math.random() * 2 * Math.PI;

  const scale = params.minScale + Math.random() * ( params.maxScale - params.minScale );
  size.set( scale, scale, scale );

  const geometry = new THREE.Mesh(
    new THREE.SphereGeometry(0.3, 15, 15),
    new THREE.MeshBasicMaterial( {color: Math.random() * 0xffffff } ),
  );
  geometry.position.set(position.x, position.y, position.z);
  decals.push( geometry );
  scene.add( geometry );
}

function showLengths() {
  decals.forEach((item, key) => {
    if (key > 0) {
      const len = Math.sqrt(
        Math.pow(decals[key-1].position.x - item.position.x, 2)
        + Math.pow(decals[key-1].position.y - item.position.y, 2)
        + Math.pow(decals[key-1].position.z - item.position.z, 2)
      );
      console.log(
        decals[key-1].position,
        item.position,
        len,
      );
    }
    // console.log(item.position);
  })
}
function removeDecals() {
  decals.forEach( function ( d ) {
    scene.remove( d );
  } );
  decals.length = 0;
}

function onWindowResize() {

  camera.aspect = window.innerWidth / window.innerHeight;
  camera.updateProjectionMatrix();

  renderer.setSize( window.innerWidth, window.innerHeight );

}

function animate() {

  requestAnimationFrame( animate );
  renderer.render( scene, camera );

}

export default function Canvas3() {
  return (
    <div/>
  )
}
