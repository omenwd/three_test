import * as THREE from "three";
import {OrbitControls} from "three/examples/jsm/controls/OrbitControls"
import {GLTFLoader} from "three/examples/jsm/loaders/GLTFLoader";

const raycaster = new THREE.Raycaster();
const pointer = new THREE.Vector2();
document.addEventListener( 'pointermove', onPointerMove );
function onPointerMove( event ) {
  pointer.x = ( event.clientX / window.innerWidth ) * 2 - 1;
  pointer.y = - ( event.clientY / window.innerHeight ) * 2 + 1;
}

let camera, scene, renderer;

init();
animate();

function getAllMeshes(object) {
  // console.log(object);
  // return [];
  return object?.children[0]?.children[0]?.children[0]?.children[0]?.children || [];
}

function init() {

  camera = new THREE.PerspectiveCamera( 45, window.innerWidth / window.innerHeight, 1, 1000 );
  camera.position.y = 200;

  scene = new THREE.Scene();
  scene.background = new THREE.Color('aliceblue');

  const ambientLight = new THREE.AmbientLight( 'aliceblue', 0.4 );
  scene.add( ambientLight );

  const pointLight = new THREE.PointLight( 'aliceblue', 0.8 );
  camera.add( pointLight );
  scene.add( camera );

  /*new MTLLoader()
    .load('/static/data_set/3d_mesh/alwasil fort 3d_simplified_3d_mesh.mtl', function (materials) {
      materials.preload();
      new OBJLoader()
        .setMaterials(materials)
        .load('/static/data_set/3d_mesh/alwasil fort 3d_simplified_3d_mesh.obj', function (object) {
          object.position.y = - 95;
          object.name="my_scene";
          var texture = new THREE.TextureLoader().load('/static/data_set/3d_mesh/alwasil fort 3d_texture.jpg');

          object.traverse(function (child) {   // aka setTexture
            if (child instanceof THREE.Mesh) {
              child.material.map = texture;
            }
          });
          scene.add(object);
        });
    });
*/

  function frameArea(sizeToFitOnScreen, boxSize, boxCenter, camera) {
    const halfSizeToFitOnScreen = sizeToFitOnScreen * 0.5;
    const halfFovY = THREE.MathUtils.degToRad(camera.fov * .5);
    const distance = halfSizeToFitOnScreen / Math.tan(halfFovY);
    // compute a unit vector that points in the direction the camera is now
    // in the xz plane from the center of the box
    const direction = (new THREE.Vector3())
      .subVectors(camera.position, boxCenter)
      .multiply(new THREE.Vector3(1, 0, 1))
      .normalize();

    // move the camera to a position distance units way from the center
    // in whatever direction the camera was from the center already
    camera.position.copy(direction.multiplyScalar(distance).add(boxCenter));

    // pick some near and far values for the frustum that
    // will contain the box.
    camera.near = boxSize / 100;
    camera.far = boxSize * 100;

    camera.updateProjectionMatrix();

    // point the camera to look at the center of the box
    camera.lookAt(boxCenter.x, boxCenter.y, boxCenter.z);
  }

  {
    const gltfLoader = new GLTFLoader();
    gltfLoader.load('/static/data_set/3d_mesh/mesh.glb', (gltf) => {
      const root = gltf.scene;
      root.name="my_scene";
      scene.add(root);

      // compute the box that contains all the stuff
      // from root and below
      const box = new THREE.Box3().setFromObject(root);

      const boxSize = box.getSize(new THREE.Vector3()).length();
      const boxCenter = box.getCenter(new THREE.Vector3());

      // set the camera to frame the box
      frameArea(boxSize * 0.5, boxSize, boxCenter, camera);

      // update the Trackball controls to handle the new size
      controls.maxDistance = boxSize * 10;
      controls.target.copy(boxCenter);
      controls.update();
    });
  }

  /*{
    const loader = new FBXLoader();
    loader.load('/static/data_set/3d_mesh/alwasil fort 3d_simplified_3d_mesh.fbx', function (object3d) {
      scene.add(object3d);
    }, undefined, function (e) {
      console.error(123, e);
    });
  }*/
  /*{
    const loader = new OBJLoader();
    loader.load('/static/data_set/3d_mesh/alwasil fort 3d_simplified_3d_mesh.obj', function (object3d) {
      scene.add(object3d);
    }, undefined, function (e) {
      console.error(123, e);
    });
  }*/
  /*{
    const loader = new MTLLoader();
    loader.load('/static/data_set/3d_mesh/alwasil fort 3d_simplified_3d_mesh.mtl', function (object3d) {
      scene.add(object3d);
    }, undefined, function (e) {
      console.error(123, e);
    });
  }*/


  renderer = new THREE.WebGLRenderer( { antialias: true } );
  const controls = new OrbitControls(camera, renderer.domElement);
  controls.target.set(0, 5, 0);
  controls.update();
  renderer.setPixelRatio( window.devicePixelRatio );
  renderer.setSize( window.innerWidth, window.innerHeight );
  document.body.appendChild( renderer.domElement );

  // stats = new Stats();
  // document.body.appendChild( stats.dom );

  //

  window.addEventListener( 'resize', onWindowResize );

}

function onWindowResize() {

  camera.aspect = window.innerWidth / window.innerHeight;
  camera.updateProjectionMatrix();

  renderer.setSize( window.innerWidth, window.innerHeight );

}

//

function animate() {

  requestAnimationFrame( animate );

  render();
  // stats.update();

}

function render() {

  // const timer = Date.now() * 0.0001;

  // camera.position.x = Math.cos( timer ) * 800;
  // camera.position.z = Math.sin( timer ) * 800;

  // camera.lookAt( scene.position );

  /*scene.traverse( function ( object ) {

    if ( object.isMesh === true ) {

      object.rotation.x = timer * 5;
      object.rotation.y = timer * 2.5;

    }

  } );*/

  raycaster.setFromCamera( pointer, camera );

  /*const intersects = raycaster.intersectObject(scene, true);
  if (intersects.length > 0) {
    const object = intersects[0].object;
    console.log(scene);
    object.material.color.set( Math.random() * 0xffffff );
  }*/


  // console.log(scene.children);
  // console.log(scene);
  if (scene.getObjectByName('my_scene')) {
    console.log(scene.getObjectByName('my_scene'));
    const children = getAllMeshes(scene.getObjectByName('my_scene'));
    const intersects = raycaster.intersectObjects(children, true);
    if (intersects.length) {
      console.log({ intersects });
      children.forEach((item, key) => {
        if (intersects.findIndex((int) => int.object.name === item.name) === -1) {
          children[key].children[0].material.color.set("black");
        }
      })
      for ( let i = 0; i < intersects.length; i ++ ) {
        intersects[ i ].object.material.color.set( "red" );
      }
    }
  }

  renderer.render( scene, camera );

}

export default function Canvas2() {
  return (
    <canvas id="my-canvas" width="5" height="5" />
  )
}
